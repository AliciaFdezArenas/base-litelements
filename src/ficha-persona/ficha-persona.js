import { LitElement, html } from 'lit-element';  

class FichaPersona extends LitElement {

	static get properties() {
		return {
			name: {type: String},			
			yearsInCompany: {type: Number},	
			personInfo: {type: String},		
			photo: {type: Object}			
		};
	}			  

  constructor () {

    super();

    this.name = "Prueba nombre";
	this.yearsInCompany = 1;
	this.photo = {
		 src:"./img/persona.jpg",
		 alt:"foto persona"
	}
	this.updatePersonInfo ();

	   
  }
  updated (changedProperties)  {
    changedProperties.forEach ((oldValue, propName) => {
    console.log("Propiedad " + propName + " cambia valor, anterior era " + oldValue);
    
   });
   if (changedProperties.has("name")){
	   console.log("Propiedad name cambia valor anterior era " + changedProperties.get("name") + " nuevo es " + this.name);
   }

   if (changedProperties.has("yearsInCompany")){
	console.log("Propiedad yearsInCompany cambia valor anterior era " + changedProperties.get("yearsInCompany") + " nuevo es " + this.yearsInCompany);
	   this.updatePersonInfo();
    }
}

	render() {
	return html`
		<div>
			<label >Nombre Completo</label>
			<input type="text" id="fname" value="${this.name}" @input="${this.updateName}"></input>
			<br />						
			<label>Años en la empresa</label>
			<input type="text" value="${this.yearsInCompany}"  @input="${this.updateYearsInCompany}"></input>
			<br />	
			<input type="text" value="${this.personInfo}"  disabled></input>
			<br />	
			<img src="${this.photo.src}" height="200" width="200" alt="${this.photo.alt}"></img>
		</div>
		`;
	}
	updateName (e){
		console.log("updateName");
		console.log("Actualizando propiedad nombre con el valor " + e.target.value);
		this.name = e.target.value;
	
	}
	updateYearsInCompany(e){
		console.log("updateYearsInCompany");
		console.log("Actualizando propiedad yearsInCompany con el valor " + e.target.value);
		this.yearsInCompany = e.target.value;
		
	}
	updatePersonInfo(){
		console.log("updatePersonInfo");
		console.log("yearsInCompany vale:" + this.yearsInCompany);
		if (this.yearsInCompany >= 7){
			this.personInfo = "lead";
		
		}else if (this.yearsInCompany >= 5 ){
			this.personInfo = "senior";
			
		}else if (this.yearsInCompany >= 3 ){
			this.personInfo = "team";
		}else{
			this.personInfo = "junior";
		}
	}

}

customElements.define('ficha-persona', FichaPersona)


