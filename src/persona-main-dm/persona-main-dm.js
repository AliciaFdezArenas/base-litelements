import { LitElement, html } from 'lit-element';  
class PersonaMainDM extends LitElement {   
  static get properties(){
      return{
          people: {type: Array}
      }
  }
constructor(){
    super();

    this.people = [
       {
         name: "Ellen Ripley",
         yearsInCompany: 10,
         profile: "Lorem ipsum dolor sit amet.",
         photo: {
           src: "./img/persona.jpg",
           alt: "Ellen Ripley"
         }
       },  {
         name: "Bruce Banner",
         yearsInCompany: 2,
         profile: "Lorem ipsum. ",
         photo: {
           src: "./img/persona.jpg",
           alt: "Bruce Banner"
         }
       }, {
         name: "Eowey",
         yearsInCompany: 5,
         profile: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
         photo: {
           src: "./img/persona.jpg",
           alt: "Eowey"
         }
       },{
         name: "Turanga Leela",
         yearsInCompany: 9,
         profile: "Lorem ipsun dolor sit.",
         photo: {
           src: "./img/persona.jpg",
           alt: "Turanga Leela"
         }
       },{
         name: "Tyrion Lannister",
         yearsInCompany: 1,
         profile: "Lorem ipsun dolor sit amet Lorem ipsum dolor sit amet.",
         photo: {
             src: "./img/persona.jpg",
             alt: "Tyrion Lannister"
         } 
       }
    ];
    
}
updated(changedProperties){
    console.log("updated");

    if(changedProperties.has("people")) {

      console.log("Ha cambiadno el valor de la propiedad people");

    this.dispatchEvent (
     new CustomEvent("people-data-updated",
      {
          detail: {
            people: this.people
          }
      }
   )
   )
  } 
}
          
}  
customElements.define('persona-main-dm', PersonaMainDM) 