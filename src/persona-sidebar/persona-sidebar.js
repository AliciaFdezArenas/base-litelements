import { LitElement, html } from 'lit-element';  
class PersonaSidebar extends LitElement {   
  
  static get properties(){
    return {
      peopleStats : {type: Object},
      people: {type: Array}
    }
  }

  constructor() {
    super();
    this.peopleStats = {};
    this.people = [];
  }
  
  render() { 
    return html`
     <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
      <aside>
        <section>
            <div>
              Hay <span class="badge-pill badge-primary">${this.peopleStats.numberOfPeople}</span> personas
            </div>
            <div class="mt-5">
                <button class="w-100 btn btn-success" style="font-size: 50px" @click="${this.newPerson}"><strong>+</strong></button>
                
            </div>
            <div>
            <input type="range" min="0" max="${this.peopleStats.maxYearsInCompany}" step="1" value="${this.peopleStats.maxYearsInCompany}" @input="${this.updateMaxYearsInCompany}"/>
          </div>
        </section>
      </aside>
  `;
  } 
  newPerson(){
    console.log("newPerson en persona-sidebar");
    console.log("Se va a crear una persona nueva");
    
     //en el detail va vacio, no es necesario enviar nada
    this.dispatchEvent(new CustomEvent("new-person", {}));
  }

updateMaxYearsInCompany(e){
    console.log("updateFilter en persona-sidebar");
    console.log("El rango vale " + e.target.value);
   this.dispatchEvent(
     new CustomEvent("update-max-years-filter",
     {
     detail: {
      maxYearsInCompany: e.target.value
     }
     }
   ));
  }
}
customElements.define('persona-sidebar', PersonaSidebar) 